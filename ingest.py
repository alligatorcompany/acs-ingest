import os
import json
import sys
import datetime
from telnetlib import AUTHENTICATION
import time
import decimal
import uuid
import dataclasses
import traceback
from dacite import from_dict
import glob
import pyexasol
from dataclasses import dataclass
from yamlreader import yaml_load
from sqlalchemy import create_engine
from sqlalchemy.engine.url import make_url
import hashlib
import concurrent.futures
import argparse
import urllib.parse
from simple_salesforce import Salesforce
import requests
from dotenv import load_dotenv
import openpyxl
import pandas as pd
import numpy as np
from office365.runtime.auth.authentication_context import AuthenticationContext
from office365.sharepoint.client_context import ClientContext
from office365.sharepoint.files.file import File
import io
load_dotenv()  # take environment variables from .env.

#since the error messages are thrown differently by Sqlalchemy and Exasol, the error codes are stored in a list that can be matched with the error string
db2_hadr_error = ['sql1776n','sqlcode=-1776','hadr','error message 20047, severity 9','dbprocess is dead or not enabled',
'sqlstate=08001', 'sqlcode=-30081']
#db2_hadr_error = ['sqlcode=-204','sqlstate=42704'] #This was for test only (give a wrong table name to force raising the error)
#how often we try to reload the table after DB2 HADR error
db2_hadr_attempts = 10
#seconds we wait for relaod after DB2 HADR error
db2_hadr_delay = 30

@dataclass(unsafe_hash=False, eq=False)
class Table:
    url: str
    source_name: str
    table_schema: str
    table_name: str
    unique_index: str
    native_import: bool
    attempts: int

    def get_unique_index(self):
        if self.unique_index is None:
            return 'None'
        else:
            return self.unique_index.lower()

    def get_bkstr(self, payload: str, row):
        if not self.unique_index or len(self.unique_index)==0:
            return payload
        else:
            bkstr = ''
            for idcol in self.unique_index.split(','):
                bkstr+=str(row[idcol.lower()])
                bkstr+='^~|'
            return bkstr


class TypeEncoder(json.JSONEncoder):
    def default(self, z):
        if isinstance(z, datetime.datetime):
            return (str(z))
        if isinstance(z, datetime.date):
            return (str(z))
        if isinstance(z, decimal.Decimal):
            return (str(z))
        if isinstance(z, uuid.UUID):
            return str(z);
        if isinstance(z, bytes):
            return str(z);
        else:
            return super().default(z)


def connect_exasol():
    psa_landing = (os.environ['EXASOL_SCHEMA'], 'PSA_LANDING')
    C = pyexasol.connect(dsn=os.environ['EXASOL_DSN'],
                         user=os.environ['EXASOL_USER'],
                         password=os.environ['EXASOL_PASS'],
                         schema=os.environ['EXASOL_SCHEMA'],
                         compression=True)
    return psa_landing, C


def clean_landing():
    psa_landing, C = connect_exasol()
    C.execute('truncate table {}.{} '.format(psa_landing[0], psa_landing[1]))
    C.close()
    print('truncated table table {}.{}!'.format(psa_landing[0], psa_landing[1]))


def convert_row(ldts: datetime.datetime, table:Table, row):
    line=[]
    row = {k.lower():v for k, v in row.items()}
    payload = json.dumps(row, cls=TypeEncoder)

    line.append(ldts)
    line.append(table.source_name)
    line.append(table.table_schema)
    line.append(table.table_name)
    line.append(table.get_unique_index())
    bkstr = table.get_bkstr(payload, row)
    line.append(hashlib.md5(bkstr.encode()).hexdigest())
    line.append(payload)
    return line


def retrieve_iter(url, auth=None, detail_name='observations'):
    while url:
        response = requests.get(url, auth=auth)
        if 200 >= response.status_code < 300:
            json_data = response.json()
            for row in json_data['value']:
                if not detail_name in row.keys():
                    key_name = '{}@odata.navigationLink'.format(detail_name)
                    valueurl = None
                    if key_name in row.keys():
                        valueurl = row.get(key_name)
                    while valueurl:
                        valueresp = requests.get(valueurl, auth=auth)
                        valuejson = valueresp.json()
                        if not detail_name in row.keys():
                            row[detail_name] = []
                        row[detail_name].extend(valuejson['value'])
                        valueurl = valuejson.get('@odata.nextLink')
                yield row
            url = json_data.get('@odata.nextLink')
        else:
            raise RuntimeError('GET "{}" returned {}: {}'.format(url, response.status_code, response.reason))


def retrieve_odata(source_name: str, connection_url: str, endpoint: str, query_args: str=None):
    BASE_URL = connection_url
    if BASE_URL[-1:] != '/':
        BASE_URL += '/'
    AUTH = (
        os.environ['{}_USER'.format(source_name).upper()],
        os.environ['{}_PASS'.format(source_name).upper()])
    query_args = '$expand=observations'
    #query_args = urllib.parse.quote(query_args)
    url = BASE_URL + endpoint + ('' if query_args is None else '?' + query_args)
    yield from retrieve_iter(url, AUTH)


def ingest_odata(ldts: datetime.datetime, table:Table):
    print('Loading table {} from {} ---'.format(table.table_name, table.source_name))
    psa_landing, C = connect_exasol()
    lines=[]
    for row in retrieve_odata(table.source_name, table.url, table.table_name):
        lines.append(convert_row(ldts, table, row))
    C.import_from_iterable(lines, psa_landing)
    C.close()
    print('Finished table {} from {} ---'.format(table.table_name, table.source_name))


def retrieveSalesforce(source_name: str, object_name: str):
    sf = Salesforce(
        username=os.environ['{}_USER'.format(source_name).upper()],
        password=os.environ['{}_PASS'.format(source_name).upper()],
        security_token=os.environ['{}_TOKEN'.format(source_name).upper()],
        version='52.0')

    query = "SELECT FIELDS(ALL) FROM " + object_name + " %s ORDER BY Id LIMIT 200"

    last_id = None
    found_one = True
    while found_one:
        id_where = '' if last_id is None else "WHERE Id > '%s'" % last_id
        data = sf.query_all_iter(query % id_where)

        found_one = False
        for row in data:
            last_id = row['Id']
            found_one = True
            yield {k.lower():v for k, v in row.items()}


def ingest_salesforce(ldts: datetime.datetime, table:Table):
    print('Loading table {} from {} ---'.format(table.table_name, table.source_name))
    psa_landing, C = connect_exasol()
    lines=[]
    for row in retrieveSalesforce(table.source_name, table.table_name):
        lines.append(convert_row(ldts, table, row))
    C.import_from_iterable(lines, psa_landing)
    C.close()
    print('Finished table {} from {} ---'.format(table.table_name, table.source_name))

def retrieve_excel(source_path:str, table_name:str):

    #get excel bytes from sharepoint

    sharepoint_user = os.environ['SHAREPOINT_USER']
    sharepoint_pass = os.environ['SHAREPOINT_PASS']
    url = source_path

    ctx_auth = AuthenticationContext(url)
    if ctx_auth.acquire_token_for_user(sharepoint_user,sharepoint_pass):
        ctx = ClientContext(url,ctx_auth)
        web=ctx.web
        ctx.load(web)
        ctx.execute_query()
        print("Sharepoint Authentication Successful")
    response = File.open_binary(ctx,url)
    bytes_file_obj = io.BytesIO()
    bytes_file_obj.write(response.content)
    bytes_file_obj.seek(0)


    #retrieve data from excel using openpyxl
    wb = openpyxl.load_workbook(bytes_file_obj, data_only=True)

    df = None
    #get worksheet names
    for ws_name in wb.sheetnames:
        if table_name.lower() == ws_name.lower():
            ws = wb[ws_name]
            data = ws.values
            columns = next(data)[0:]
            df = pd.DataFrame(data,columns=columns).rename(columns=str.lower).replace('-','')
    if df is not None:
        return df
    else:
        print('unable to load excel sheet {} from file {}'.format(table_name,source_path))

def ingest_excel(ldts: datetime.datetime, table:Table):
    #execute ingest process
    print('Loading table {} from {} ---'.format(table.table_name, table.source_name))
    df = retrieve_excel(source_path=table.url,table_name=table.table_name)
    psa_landing, C = connect_exasol()
    lines=[]
    for label,row in df.iterrows():
        lines.append(convert_row(ldts, table, row))
    C.import_from_iterable(lines, psa_landing)
    C.close()
    print('Finished table {} from {} ---'.format(table.table_name, table.source_name))


def retrieveRelational(source_name: str, connection_url:str, table_schema:str, table_name:str):
    url = make_url(connection_url)
    url = url.create(drivername=url.drivername,
        username=os.environ['{}_USER'.format(source_name).upper()],
        password=os.environ['{}_PASS'.format(source_name).upper()],
        host=url.host,
        port=url.port,
        database=url.database)

    engine = create_engine(url)
    q = 'SELECT * FROM {}.{}'.format(table_schema, table_name)
    con = engine.connect()
    return con.execution_options(stream_results=True).execute(q)


def ingest_relational(ldts: datetime.datetime, table:Table):
    try:
        table.attempts +=1
        if table.attempts == 1:
            print('Loading table {} from {} ({})---'.format(table.table_name, table.source_name,datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')))
        dt_start = datetime.datetime.now()
        psa_landing, C = connect_exasol()
        proxy = retrieveRelational(table.source_name, table.url, table.table_schema, table.table_name)
        while 'batch not empty':  # equivalent of 'while True', but clearer
            batch = proxy.fetchmany(10000)
            if not batch:
                break
            lines=[]
            for row in batch:
                lines.append(convert_row(ldts, table, row))
            C.import_from_iterable(lines, psa_landing)
        proxy.close()
        C.close()
        print('Finished table {} from {} Duration: {} ---'.format(table.table_name, table.source_name,get_timedelta(dt_start,datetime.datetime.now())))
    except:
        if any(ele in traceback.format_exc(chain=False).lower() for ele in db2_hadr_error) == True and table.attempts <= db2_hadr_attempts:
            time.sleep(db2_hadr_delay)
            print('{}. retry to load table {} from {} ---'.format(table.attempts, table.table_name, table.source_name))
            ingest_relational(ldts=ldts, table=table)
        traceback.print_exc(limit=1,chain=False)
        print('Exception on table {} from {} Duration: {} ---'.format(table.table_name, table.source_name,get_timedelta(dt_start,datetime.datetime.now())))

def ingest_native(table: Table):
    try:
        table.attempts +=1
        if table.attempts == 1:
            print('Loading table {} from {} ({})---'.format(table.table_name, table.source_name,datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')))
        dt_start = datetime.datetime.now()
        psa_landing, C = connect_exasol()
        url = make_url(table.url)
        url = url.create(drivername=url.drivername,
            username=os.environ['{}_USER'.format(table.source_name).upper()],
            password=os.environ['{}_PASS'.format(table.source_name).upper()],
            host=url.host,
            port=url.port,
            database=url.database)

        url.set(username=os.environ['{}_USER'.format(table.source_name).upper()], password=os.environ['{}_PASS'.format(table.source_name).upper()])
        import_jdbc = 'create or replace table {}.{} as (select * from ( import from JDBC at {} statement ' \
                '\'select * from {}.{}\'));'.format(
                psa_landing[0],table.table_name,table.source_name ,table.table_schema, table.table_name)
        append_landing = 'insert into {}.psa_landing ' \
             'select ldts, source_name, interface_schema, interface_name, \'{}\', hash_md5(bk_md5), payload ' \
             'from (select current_timestamp as ldts, \'{}\' as source_name, \'{}\' as interface_schema, \'{}\' as interface_name, "{}"."rel2json"(\'{}\',\'{}\', \'{}\' ))'.format(
            psa_landing[0], table.unique_index, table.source_name, table.table_schema, table.table_name, psa_landing[0], psa_landing[0],
            table.table_name, table.unique_index)
        C.execute(import_jdbc)
        C.execute(append_landing)
        print('Finished table {} from {} Duration: {} ---'.format(table.table_name, table.source_name,get_timedelta(dt_start,datetime.datetime.now())))
    except:
        if any(ele in traceback.format_exc(chain=False).lower() for ele in db2_hadr_error) == True and table.attempts <= db2_hadr_attempts:
            time.sleep(db2_hadr_delay)
            print('{}. retry to load table {} from {} ---'.format(table.attempts, table.table_name, table.source_name))
            ingest_native(table=table)
        traceback.print_exc(limit=1,chain=False)
        print('Exception on table {} from {} Duration: {} ---'.format(table.table_name, table.source_name,get_timedelta(dt_start,datetime.datetime.now())))


def get_timedelta(dt_start: datetime, dt_end: datetime):
    try:
        return dt_end - dt_start
    except:
        return datetime.timedelta(seconds=0)

def readFromProject(projectdir: str, sources):
    yamls = glob.glob('{}/**/*.yml'.format(projectdir), recursive=True)
    modelfiles = []
    for yml in yamls:
        if not yml.endswith(('packages.yml','profiles.yml','dbt_project.yml')) and not ('target') in yml and not 'dbt_modules' in yml:
            modelfiles.append(yml)
    models = yaml_load(modelfiles)
    tables = []
    for source in models['sources']:
        if 'meta' in source.keys()  and source['name'] in sources:
            if 'dburl' in source['meta'].keys():
                if 'tables' in source.keys():
                    for table in source['tables']:
                        if 'meta' in table.keys():
                            table['meta'].setdefault('native_import',False)
                            table['meta'].setdefault('attempts',0)
                            tables.append(
                                Table(
                                    source['meta']['dburl'],
                                    source['name'],
                                    table['meta']['source_schema'],
                                    table['meta']['source_name'],
                                    table['meta']['unique_index'],
                                    table['meta']['native_import'],
                                    table['meta']['attempts']))
    return tables


def list(projectdir: str):
    tables = readFromProject(projectdir)
    dicts = []
    for table in tables:
        dicts.append(dataclasses.asdict(table))
    return dicts

def ingest(tables,  ldts: datetime):
    if ldts == None:
        ldts = (datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f'))
    with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:
        futures = []
        for table in tables:
            if table.source_name in ['sf']:
                futures.append(
                    executor.submit(ingest_salesforce, ldts=ldts, table=table)
                )
            elif table.source_name in ['ihs']:
                futures.append(
                    executor.submit(ingest_odata, ldts=ldts, table=table)
                )
            elif table.source_name in ['inno_excel']:
                futures.append(
                    executor.submit(ingest_excel, ldts=ldts, table=table)
                )
            else:
                if table.native_import == True:
                    futures.append(
                        executor.submit(ingest_native, table=table)
                    )
                else:
                    futures.append(
                        executor.submit(ingest_relational, ldts=ldts, table=table)
                )
        for future in concurrent.futures.as_completed(futures):
            if future.result() != None:
                print(future.result())

def main():

    project = sys.argv[1]
    sources = sys.argv[2].split(',')

    print("Ingesting {} from project {}...".format(sources, project))
    tables = readFromProject(project, sources )
    clean_landing()
    ldts = (datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f'))
    ingest(tables=tables, ldts=ldts)

if __name__ == '__main__':
    sys.exit(main())
