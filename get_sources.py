import sys
import json
import glob
from dataclasses import dataclass
import csv

@dataclass(unsafe_hash=False, eq=False)
class Table:
    url: str
    source_name: str
    identifier: str
    table_schema: str
    table_name: str
    unique_index: str
    native_import: bool

def readFromProject(projectdir: str):
    jsons = glob.glob('{}/**/manifest.json'.format(projectdir), recursive=True)
    modelfiles = [] 
    for json in jsons:
        if ('target') in json and not ('dbt_modules') in json:
            modelfiles.append(json)
    return modelfiles

def get_metadata(json_file: str):
    meta = []
    sources = []
    json_data = json.load(open(json_file))
    sources = json_data['sources']
    for source in sources:
        json_data['sources'][source]['meta'].setdefault('native_import',False)
        json_data['sources'][source]['source_meta'].setdefault('dburl','')
        json_data['sources'][source]['meta'].setdefault('source_schema','')
        json_data['sources'][source]['meta'].setdefault('source_name','')
        json_data['sources'][source]['meta'].setdefault('unique_index','')
        meta.append(Table
            (
                json_data['sources'][source]['source_meta']['dburl'],
                json_data['sources'][source]['source_name'],
                json_data['sources'][source]['identifier'],
                json_data['sources'][source]['meta']['source_schema'],
                json_data['sources'][source]['meta']['source_name'],
                json_data['sources'][source]['meta']['unique_index'],
                json_data['sources'][source]['meta']['native_import'],
                )
            )
    return meta

def main():
    project = sys.argv[1]
    output_dir = sys.argv[2] if 2 < len(sys.argv) else '.'

    sources = readFromProject(project)

    meta_data = [get_metadata(x) for x in sources]
    header = []
    data_rows = []

    for tables in meta_data:
        for table in tables:
            data = []
            table_dict = vars(table)
            for field in table_dict:
                if not field in header:
                    header.append(field)
                data.append(table_dict[field])
            if not data in data_rows:
                data_rows.append(data)

    with open('{}/dbt_sources.csv'.format(output_dir), 'w', encoding='UTF8', newline='') as f:
        writer = csv.writer(f, delimiter = ";", quoting = csv.QUOTE_NONNUMERIC)

        # write the header
        writer.writerow(header)

        # write multiple rows
        writer.writerows(data_rows)

if __name__ == '__main__':
    sys.exit(main())
