FROM python:3.9.7-slim-bullseye as base
WORKDIR /app

FROM base as builder
ENV DEBIAN_FRONTEND=noninteractive
RUN apt update && apt install -y build-essential unixodbc-dev && rm -rf /var/lib/apt/lists/*
ENV PIP_DISABLE_PIP_VERSION_CHECK=on
RUN pip install poetry
COPY poetry.lock pyproject.toml /app/
RUN poetry config virtualenvs.create true
RUN poetry config virtualenvs.path "/venv/"
RUN poetry install --no-interaction

FROM base as final
RUN apt update && apt install -y libxml2 &&  rm -rf /var/lib/apt/lists/*
COPY --from=builder /venv /venv
COPY ingest.py /app
